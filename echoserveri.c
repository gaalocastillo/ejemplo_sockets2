#include "csapp.h"
#include <unistd.h> 
#include <signal.h>
#include "csapp.h"
#include <errno.h>
void echo(int connfd);
void sigchld_handler(int sig);

int main(int argc, char **argv){
	
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];
	
	Signal(SIGCHLD, sigchld_handler);

	listenfd = Open_listenfd(port);

	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		if (Fork()==0){

			Close(listenfd);
			/* Determine the domain name and IP address of the client */
			hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
						sizeof(clientaddr.sin_addr.s_addr), AF_INET);
			haddrp = inet_ntoa(clientaddr.sin_addr);
			printf("server connected to %s (%s)\n", hp->h_name, haddrp);
			echo(connfd);
			Close(connfd);
			exit(0);

		}
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	int status;
	pid_t p;
	char * args[2];
	size_t n;
	char buf[MAXLINE];
	char * comandoString;
	char * mensaje;
	//char * comandoString;
	
	rio_t rio;
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("server received %lu bytes\n", n);
//		comandoString=malloc(strlen(buf)+1);
//		strcpy(comandoString,buf);
//		comandoString[strlen(comandoString)-1] = 0;
    	if ((strlen(buf)>0) && (buf[strlen (buf) - 1] == '\n')){
        	buf[strlen (buf)-1] = '\0';
    	} 
//    	if ((strlen(comandoString)>0) && (str[strlen (comandoString) - 1] == '\n')){
//        	comandoString[strlen (comandoString)-1] = '\0';
//    	}
    	mensaje = "ERROR\n";
    	char *const args[] = {buf, NULL};
		if ((p=fork())==0){
			if(execve(buf,args,NULL)<0){
				raise(SIGABRT);
				Rio_writen(connfd, mensaje, strlen(mensaje));
				exit(1);
			}
		}else{

			waitpid(p,&status,0);
			if (status!=0){
				Rio_writen(connfd, mensaje, strlen(mensaje));
			}
			else{
				mensaje="OK\n";
				Rio_writen(connfd, mensaje, strlen(mensaje));
			}
		}	
		
	}
	exit(0);
}

void sigchld_handler(int sig){
	int status;
	while (waitpid(-1, &status, WNOHANG) > 0){
		if(WIFEXITED(status)){
			printf("EL hijo fue eliminado\n");

		}
		else{
			printf("Error\n");
		}
	}
}
